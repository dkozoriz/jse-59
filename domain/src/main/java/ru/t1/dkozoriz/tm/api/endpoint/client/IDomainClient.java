package ru.t1.dkozoriz.tm.api.endpoint.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkozoriz.tm.api.IEndpointClient;
import ru.t1.dkozoriz.tm.dto.request.data.load.*;
import ru.t1.dkozoriz.tm.dto.request.data.save.*;
import ru.t1.dkozoriz.tm.dto.response.data.load.*;
import ru.t1.dkozoriz.tm.dto.response.data.save.*;

public interface IDomainClient extends IEndpointClient {

    @NotNull
    DataBackupLoadResponse loadBackup(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse saveBackup(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveBase64(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadBinary(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveBinary(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse loadYaml(@NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse saveYaml(@NotNull DataYamlSaveFasterXmlRequest request);

    @NotNull
    DataJsonLoadFasterResponse loadJsonFaster(@NotNull DataJsonLoadFasterRequest request);

    @NotNull
    DataJsonSaveFasterResponse saveJsonFaster(@NotNull DataJsonSaveFasterRequest request);

    @NotNull
    DataJsonLoadJaxBResponse loadJsonJaxB(@NotNull DataJsonLoadJaxBRequest request);

    @NotNull
    DataJsonSaveJaxBResponse saveJsonJaxB(@NotNull DataJsonSaveJaxBRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse loadXmlFaster(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse saveXmlFaster(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxBResponse loadXmlJaxB(@NotNull DataXmlLoadJaxBRequest request);

    @NotNull
    DataXmlSaveJaxBResponse saveXmlJaxB(@NotNull DataXmlSaveJaxBRequest request);

}
