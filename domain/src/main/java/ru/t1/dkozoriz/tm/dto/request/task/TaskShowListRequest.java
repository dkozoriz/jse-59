package ru.t1.dkozoriz.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.dto.request.AbstractUserRequest;
import ru.t1.dkozoriz.tm.enumerated.Sort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class TaskShowListRequest extends AbstractUserRequest {

    @Nullable
    private Sort sort;

    public TaskShowListRequest(
            @Nullable final String token,
            @Nullable Sort sort
    ) {
        super(token);
        this.sort = sort;
    }

}