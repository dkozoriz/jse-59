package ru.t1.dkozoriz.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskListClearRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;

@Component
public final class TaskListClearListener extends AbstractTaskListener {

    public TaskListClearListener() {
        super("task-clear", "delete all tasks.");
    }

    @Override
    @EventListener(condition = "@taskListClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[CLEAR TASKS]");
        taskEndpoint.taskListClear(new TaskListClearRequest(getToken()));
    }

}