package ru.t1.dkozoriz.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.api.endpoint.ISystemEndpoint;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.listener.AbstractListener;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @Autowired
    protected ISystemEndpoint systemEndpoint;

    public AbstractSystemListener(@NotNull String name, @Nullable String description) {
        super(name, description);
    }

    public AbstractSystemListener(@NotNull String name, @Nullable String description, @Nullable String argument) {
        super(name, description, argument);
    }

    public Role[] getRoles() {
        return null;
    }

}