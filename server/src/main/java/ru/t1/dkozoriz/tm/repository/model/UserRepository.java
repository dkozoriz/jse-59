package ru.t1.dkozoriz.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.dkozoriz.tm.api.repository.model.IUserRepository;
import ru.t1.dkozoriz.tm.dto.model.UserDto;
import ru.t1.dkozoriz.tm.model.User;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @NotNull
    protected Class<User> getClazz() {
        return User.class;
    }

    @Override
    @Nullable
    public User findByLogin(@Nullable final String login) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    @Nullable
    public User findByEmail(@Nullable final String email) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return findAll()
                .stream()
                .anyMatch(u -> login.equals(u.getLogin()));
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return findAll()
                .stream()
                .anyMatch(u -> email.equals(u.getEmail()));
    }

}