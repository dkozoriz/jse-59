package ru.t1.dkozoriz.tm.repository.model.business;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.api.repository.model.IProjectRepository;
import ru.t1.dkozoriz.tm.model.business.Project;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectRepository extends BusinessRepository<Project> implements IProjectRepository {

    @Override
    @NotNull
    protected Class<Project> getClazz() {
        return Project.class;
    }

}