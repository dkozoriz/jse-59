package ru.t1.dkozoriz.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.dto.model.UserDto;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionDtoRepository extends UserOwnedDtoRepository<SessionDto> implements ISessionDtoRepository {

    @Override
    @NotNull
    protected Class<SessionDto> getClazz() {
        return SessionDto.class;
    }

}